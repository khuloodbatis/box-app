<?php

namespace App\Http\Controllers;

use App\Models\Box;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use  App\Http\Resources\Box as BoxResources;
use App\Http\Controllers\BaseController as BaseController;

class BoxController extends BaseController
{

    public function index()
    {
        $boxs = Box::all();
        return $this->sendResponse(BoxResources::collection($boxs), 'Boxs  retrieved successfully!');
    }


    public function userBox($id)
    {
        $boxs = Box::where('user_id', $id)->get();

        return $this->sendResponse(BoxResources::collection($boxs), 'Boxs  retrieved successfully!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [

            'name' => 'required',
            'description' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validate Error', $validator->errors());
        }
        $user = Auth::user();
        $input['user_id'] = $user->id;
        $box = Box::create($input);
        return $this->sendResponse($box, 'box added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Box  $box
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $box = Box::find($id);
        if (is_null($box)) {
            return $this->sendError('Box not found');
        }

        return $this->sendResponse(new BoxResources($box), 'Box  retrieved successfully!');
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Box  $box
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Box $box)
    {
        $input = $request->all();
        $validator = Validator::make($request->all(), [

            'name' => 'required',
            'description' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error', $validator->errors());
        }

        if ($box->user_id != Auth::id()) {
            return $this->sendError('you dont have rights', $validator->errors());
        }

        $box->name = $input['name'];
        $box->description = $input['description'];
        $box->save();



        return $this->sendResponse(new BoxResources($box), 'Box  updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Box  $box
     * @return \Illuminate\Http\Response
     */
    public function destroy(Box $box)
    {
        if ($box->user_id != Auth::id()) {
            return $this->sendError('you dont have rights', $validator->errors());
        }
        $box->delete();

        return $this->sendResponse(new BoxResources($box), 'Box  deleted successfully!');
    }
}
